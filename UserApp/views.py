from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import UserAuthenticationForm, RegisterForm, UserUpdateForm, ProfileUpdateForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required


# Create your views here.

#view for user to register
def register_view(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = RegisterForm()
    return render(request, 'UserApp/register.html', {'form': form})

#make it possible for a user to logout
def logout_view(request):
    logout(request)
    return redirect('showcase')

#make it poossible for user to login
def login_view(request):
    context = {}
    user = request.user

    if user.is_authenticated:
        return redirect('showcase')

    if request.method == 'POST':
        form = UserAuthenticationForm(request.POST)

        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)

            if user:
                login(request, user)
                return redirect('showcase')
    else:
        form = UserAuthenticationForm()

    context['LoginForm'] = form
    return render(request, 'UserApp/login.html', context)

#allow user to create their profile
@login_required
def Profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)

        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Profile Updated Successfully')
            return redirect('profile')

    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'u_form': u_form,
        'p_form': p_form
    }
    return render(request, 'UserApp/profile.html', context)
