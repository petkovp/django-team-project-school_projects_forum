from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate
from database_manager.models import User, Profile

#form for user to register
class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['email', 'username', 'name']

#form for user to authenticate
class UserAuthenticationForm(forms.ModelForm):
    password = forms.CharField(label='Password', widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ['username', 'password']

    def clean(self):
        username = self.cleaned_data['username']
        password = self.cleaned_data['password']
        if not authenticate(username=username, password=password):
            raise forms.ValidationError("invalid login")

#form for user to update their info
class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'name']

#update a user' profile pic
class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['image']


