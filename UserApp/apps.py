from django.apps import AppConfig


class UserAppConfig(AppConfig):
    name = 'UserApp'

    def ready(self):
        import UserApp.signals
