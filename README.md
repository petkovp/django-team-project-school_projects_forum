URL to the project : https://projects-showcase-prj2-grp5.herokuapp.com/Showcase/

Our website presents multiple projects authored by many different people. You may view the project's detail by clicking on the details button.

We also implemented a search function that allows you to search for a specific project, whether it's by a keyword, oldest or most recent project!

Projects Details: You may like, flag or comment on the project IF you are a registered user.

Create Projects : If you're a registered user, you can create a project here. You can even insert an image!

View My Projects : If you made projects, you may edit or delete them at your own discretion. 

superuser : nasr 

pw : 123

superuser nasr has special tabs. He can:

Edit all project even if he's not the authored

Edit all accounts : Allocate new or remove privileges or even delete them!

He can create new admin accounts that can edit all projects or new admin accounts that can edit other user accounts (except the nasr account)

My Account : The user may change his display picture, change password, edit his info.

Login/Register : You can login or access the register page here. If you forgot your password, you may request do that too.

