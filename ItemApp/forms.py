from django import forms
from database_manager.models import Comment, Project, User

#from to create a new comment
class NewCommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['title', 'comment','rate']

#form to create a new Project
class NewProjectForm(forms.ModelForm):
    description = forms.TextInput(attrs={'rows': 1,
                                         'cols': 40,
                                         'style': 'height: 1em;'})
    class Meta:
        model = Project
        fields = [ 'project_title', 'project_type', 'keyword_list', 'description', 'url', 'status', 'snapshot']
