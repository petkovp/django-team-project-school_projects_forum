from django.urls import path
from .views import GetProjectsList, GetProjectDetail, GetUserProjectsList, CreateProject, LikeView, FlagView, UpdateProject, DeleteProject
from django.views.generic import RedirectView

urlpatterns = [
    path("", RedirectView.as_view(pattern_name='showcase')),
    path("Showcase/", GetProjectsList.as_view(), name="showcase"),
    path('Showcase/<int:pk>/', GetProjectDetail.as_view(), name="project"),
    path('Showcase/User/', GetUserProjectsList.as_view(), name="user_projects"),
    path('Showcase/User/<int:pk>/', GetProjectDetail.as_view(), name="project"),
    path('Showcase/User/create-project/', CreateProject.as_view(), name='create_project'),
    path('Showcase/User/update-project/<int:pk>/', UpdateProject.as_view(), name='update_project'),
    path('Showcase/User/delete-project/<int:pk>/', DeleteProject.as_view(), name='delete_project'),
    path('Showcase/User/like/<int:pk>/', LikeView, name="like_name"),
    path('Showcase/User/flag/<int:pk>/', FlagView, name="flag_name")
]
