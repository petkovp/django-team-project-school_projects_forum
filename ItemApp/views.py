from django.contrib import messages
from django.shortcuts import redirect, render
from django.views.generic import TemplateView, ListView, DetailView, View, DeleteView, CreateView, UpdateView, DeleteView
from database_manager.models import Project, User, Comment
from .forms import NewCommentForm, NewProjectForm
from django.views.generic.edit import FormMixin
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404


# Create your views here.

#get all the projects
class GetProjectsList(ListView):
    model = Project
    template_name = 'ItemApp/project_list.html'
    paginate_by = 8

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    #filter the projects
    #seperate it in 3 , search, date, filter. 
    def get_queryset(self):
        qs = Project.objects.all()
        search = self.request.GET.get("search")
        my_date = self.request.GET.get("date")
        my_filter = self.request.GET.get("filter")

        #filter by date
        if my_date == "recent":
            qs = Project.objects.order_by('-publish_date')

        elif my_date == "oldest":
            qs = Project.objects.order_by('publish_date')

        #filter by liked flagged or  your own posts only
        if my_filter == "liked":
            qs = Project.objects.filter(likes=self.request.user)

        elif my_filter == "flagged":
            qs = Project.objects.filter(flag=self.request.user)

        elif my_filter == "my_posts":
            qs = Project.objects.filter(owner=self.request.user)

        #filter by search bar
        if search:
            #search if there is text
            if len(search) != 0:
                qs = qs.filter(project_title__contains=search) | qs.filter(keyword_list__contains=search) | qs.filter(
                    project_type__contains=search) | qs.filter(owner__name__contains=search)
        return qs

#get the details of the a single project
class GetProjectDetail(FormMixin, DetailView):
    model = Project
    template_name = 'ItemApp/project_detail.html'
    form_class = NewCommentForm

    #get the details  and comments of a specific project
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['project'] = Project.objects.get(pk=self.kwargs['pk'])
        context['comments'] = Comment.objects.filter(project=self.kwargs['pk'])
        #always give the chance for a new comment
        context['form'] = NewCommentForm()

        #make like like a boolean 
        likes_connected = get_object_or_404(Project, id=self.kwargs['pk'])
        liked = False
        if likes_connected.likes.filter(id=self.request.user.id).exists():
            liked = True

        context['number_of_likes'] = likes_connected.number_of_likes()
        context['post_is_liked'] = liked

        # this is to calculatethe average of a post through its comments
        sumRate = 0
        counter = 0
        for com in context['comments']:
            sumRate = sumRate + com.rate
            counter = counter + 1

        if counter != 0:
            sumRate = sumRate / counter
        else:
            sumrate = -1
        context['number_of_rate'] = round(sumRate, 2)

        #make the flag like a boolean
        flag_connected = get_object_or_404(Project, id=self.kwargs['pk'])
        flagged = False
        if flag_connected.flag.filter(id=self.request.user.id).exists():
            flagged = True
        context['post_is_flagged'] = flagged

        return context

    # Handle POST GTTP requests
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    #check if the comment is valid
    def form_valid(self, form):
        comment = form.save(commit=False)
        #get the user who commented
        comment.user = self.request.user
        comment.project = Project.objects.get(pk=self.kwargs['pk'])
        comment.save()
        return super(GetProjectDetail, self).form_valid(form)

    #send it back to the same project
    def get_success_url(self):
        return reverse('project', kwargs={'pk': self.object.id})

#get all the projects made by the user only
class GetUserProjectsList(ListView):
    model = Project
    template_name = 'ItemApp/user_projects.html'
    paginate_by = 8

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    # get only the users posts
    def get_queryset(self):
        qs = Project.objects.filter(owner=self.request.user)
        return qs

# allows a user to create a project
class CreateProject(CreateView):
    template_name = 'ItemApp/create_project.html'
    form_class = NewProjectForm

    #create the project
    def form_valid(self, form):
        Project = form.save(commit=False)
        #get the project creator
        Project.owner = self.request.user
        Project.save()
        return redirect('project', Project.pk)

# allows user to update a project
class UpdateProject(UpdateView):

    template_name = 'ItemApp/edit_project.html'
    form_class = NewProjectForm

    #update the current project
    def get_object(self):
        id_ = self.kwargs.get("pk")
        return get_object_or_404(Project, pk=id_)

    #check if update is valid
    def form_valid(self, form):
        Project = form.save(commit=False)
        Project.save()
        return redirect('project', Project.pk)

    #make sure the user is looking at its own project
    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if not self.request.user.is_project_admin and obj.owner != self.request.user:
            raise Http404("You are not allowed to edit this Post")
        return super(UpdateProject, self).dispatch(request, *args, **kwargs)

# allows a user to delete a project
class DeleteProject(DeleteView):
    template_name = 'ItemApp/delete_project.html'
    model = Project
    #return to the all projects list
    success_url = reverse_lazy('showcase')

    #make sure the user is looking at its own project
    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if not self.request.user.is_project_admin and obj.owner != self.request.user:
            raise Http404("You are not allowed to Delete this Post")
        return super(DeleteProject, self).dispatch(request, *args, **kwargs)

#make the like button like a boolean
def LikeView(request, pk):
    post = get_object_or_404(Project, id=request.POST.get('like_id'))
    if post.likes.filter(id=request.user.id).exists():
        post.likes.remove(request.user)
    else:
        post.likes.add(request.user)
    return HttpResponseRedirect(reverse('project', args=[str(pk)]))

#make the like button like a boolean
def FlagView(request, pk):
    post = get_object_or_404(Project, id=request.POST.get('flag_id'))
    if post.flag.filter(id=request.user.id).exists():
        post.flag.remove(request.user)
    else:
        post.flag.add(request.user)
    return HttpResponseRedirect(reverse('project', args=[str(pk)]))