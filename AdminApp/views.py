from django.shortcuts import render, redirect
from django.views.generic import DeleteView, UpdateView, ListView
from database_manager.models import User, Project
from .forms import UpdateUserForm, NewAdminForm
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from django.contrib import messages


# Create your views here.

#allows admin to delete users
class DeleteUser(DeleteView):
    template_name = 'AdminApp/delete_user.html'
    model = User
    #after deleting a user return to all the projects
    success_url = reverse_lazy('showcase')

#allows the Admin to update a user information
class UpdateUser(UpdateView):
    template_name = 'AdminApp/update_user.html'
    form_class = UpdateUserForm

    #get the user's post
    def get_object(self):
        id_ = self.kwargs.get("pk")
        return get_object_or_404(User, pk=id_)

    #check if the updated values are proper
    def form_valid(self, form):
        User = form.save(commit=False)
        User.owner = self.request.user
        User.save()
        return redirect('adminUsers')

# form to create a new user by the admin
def NewAdminUser(request):
    if request.method == 'POST':
        form = NewAdminForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, f'Account created successfully')
            return redirect('new_admin_user')
    else:
        form = NewAdminForm()
    return render(request, 'AdminApp/register_admin.html', {'form': form})

# get all the projects for the admin
class GetAdminProjectsList(ListView):
    model = Project
    template_name = 'AdminApp/admin_project.html'
    paginate_by = 8

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


# get all the users for the admin
class GetAdminUsersList(ListView):
    model = User
    template_name = 'AdminApp/admin_users.html'
    paginate_by = 8

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
    

