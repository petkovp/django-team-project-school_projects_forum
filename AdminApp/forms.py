from database_manager.models import User
from django import forms
from django.contrib.auth.forms import UserCreationForm

#allows the Admin to update a user information
class UpdateUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['name', 'password', 'email', 'username', 'is_user_admin', 'is_project_admin']

#allows the Admin to create a brand new admin
class NewAdminForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['email', 'username', 'name', 'is_user_admin', 'is_project_admin']
