from django.contrib import admin
from database_manager.models import User
from database_manager.models import Project

#registering our models
admin.site.register(User)
admin.site.register(Project)

