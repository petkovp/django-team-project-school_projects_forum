from django.forms import forms
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser


# Create your models here.

#function to check the rate is below 5
def check_rate(num):
    if num > 5:
        return False
    return True

# class to create a new user
class MyUserManager(BaseUserManager):

    def create_user(self, email, username, name, password=None):
        #error if user is not correct
        if not email:
            raise ValueError("User must have an email")
        if not username:
            raise ValueError("User must have a username")
        if not name:
            raise ValueError("User must have a name")

        """
        Creates and saves a User with the given email, date of
        birth and password.
        """

        user = self.model(
            email=self.normalize_email(email),
            username=username,
            name=username,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, name, password=None):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email=self.normalize_email(email),
            username=username,
            name=username,
        )
        #give the admin all privileges
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.is_project_admin = True
        user.is_user_admin = True
        user.set_password(password)
        user.save(using=self._db)

        return user

# model for a user 
class User(AbstractBaseUser):
    email = models.EmailField(verbose_name='email', max_length=60, unique=True)
    username = models.CharField(max_length=32, unique=True)
    date_joined = models.DateTimeField(verbose_name='date joined,', default=timezone.now)
    last_login = models.DateTimeField(verbose_name='last login', default=timezone.now)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_user_admin = models.BooleanField(default=False)
    is_project_admin = models.BooleanField(default=False)
    name = models.CharField(max_length=200, null=False, default="")
    status = models.IntegerField(default=0)

    USERNAME_FIELD = 'username'

    REQUIRED_FIELDS = ['email', 'name']

    objects = MyUserManager()

    def __str__(self):  # __unicode__ on Python 2
        return self.name

    def get_username(self):
        return self.username

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return self.is_admin

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

#model for a user's profile one to one relationship
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')

    def __str__(self):
        return f'{self.user.username} Profile'

# the status of a project
STATUS = (
    ("Ongoing", "Ongoing"),
    ("Planned", "Planned"),
    ("Completed", "Completed")
)
#the different rating choices
RATE_CHOICES = (
    (0, 0),
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
)

#class for a project that a single user can have
class Project(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    project_type = models.CharField(max_length=200)
    project_title = models.CharField(max_length=200, default="")
    keyword_list = models.CharField(max_length=1000)
    description = models.CharField(max_length=300)
    url = models.CharField(max_length=1000, blank=True)
    status = models.CharField(max_length=100, choices=STATUS, default="Ongoing")
    snapshot = models.ImageField(default=None, upload_to='details_pic', blank=True, null=True)
    publish_date = models.DateTimeField(default=timezone.now)
    likes = models.ManyToManyField(User, related_name="like_name")
    flag = models.ManyToManyField(User, related_name="flag_name")

    def number_of_likes(self):
        return self.likes.count()

#automatically get current date
class AutoDateTimeField(models.DateTimeField):
    def pre_save(self, model_instance, add):
        return timezone.now()

#model for a comment on a post by a user
class Comment(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=50, default="")
    comment = models.TextField(verbose_name="Write A Comment", max_length=500)
    create_date = models.DateTimeField(default=timezone.now)
    rate = models.IntegerField(validators=[check_rate], default=5, choices=RATE_CHOICES)
