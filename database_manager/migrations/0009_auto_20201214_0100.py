# Generated by Django 3.1.4 on 2020-12-14 06:00

import database_manager.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('database_manager', '0008_project_flag'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='project',
            name='rate',
        ),
        migrations.AddField(
            model_name='comment',
            name='rate',
            field=models.IntegerField(choices=[(0, 0), (1, 1), (2, 2), (3, 3), (4, 4), (5, 5)], default=5, validators=[database_manager.models.check_rate]),
        ),
    ]
